package pairProgrammingCalculatorTest;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import pairProgrammingCalculator.StringCalculator;

import static org.junit.Assert.assertEquals;

public class StringCalculatorTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void dlaPustegoStringaZwracaZero(){
        expectedException.expect(NumberFormatException.class);
        StringCalculator.dodaj("");
    }

//    @Test
//    public void zwracaWyjatekDlaWiecejNizDwochArgumentow(){
//        expectedException.expect(IllegalArgumentException.class);
//        StringCalculator.dodaj("1,2,3");
//    }

    @Test
    public void zwracaSume() {
        assertEquals(25, StringCalculator.dodaj("5?2?3?1?1111?5?2?3?1?1?2"));
    }

}
