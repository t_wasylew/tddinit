package pairProgrammingCalculator;

public class StringCalculator {

    public static int dodaj(String liczba) {

        if (liczba.isEmpty()) {
            throw new NumberFormatException();
        }
        if (liczba.contains("-")) {
            throw new NumberFormatException("Negatives not allowed");
        }
        String[] split = liczba.trim().split("\\W");
        if (split[1].isEmpty()) {
            int x = Integer.parseInt(split[0]);
            return x;
        } else {
            int suma = 0;
            for (int i = 0; i < split.length; i++) {
                if (Integer.parseInt(split[i]) < 1000) {
                    suma += Integer.parseInt(split[i]);
                }
            }
            return suma;
        }
    }
}
