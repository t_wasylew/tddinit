package pl.sda.tdd.test.zadanie;

import org.junit.Test;
import pl.sda.tdd.zadanie.LiczbaPierwsza;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class LiczbaPierwszaTest {
    @Test
    public void trzeJestLiczbaPierwsza() {
        boolean czyLiczbaPierwsza = LiczbaPierwsza.czyLiczbaPierwsza(3);
        assertTrue(czyLiczbaPierwsza);
    }

    @Test
    public void jedenNieJestLiczbaPierwsza() {
        assertFalse(LiczbaPierwsza.czyLiczbaPierwsza(1));
    }

    @Test
    public void minusJedenNieJestLiczbaPierwsza() {
        assertFalse(LiczbaPierwsza.czyLiczbaPierwsza(-1));
    }

    @Test
    public void zeroNieJestLiczbaPierwsza() {
        assertFalse(LiczbaPierwsza.czyLiczbaPierwsza(0));
    }
}
