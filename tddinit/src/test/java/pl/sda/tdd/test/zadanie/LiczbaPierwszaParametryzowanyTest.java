package pl.sda.tdd.test.zadanie;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.sda.tdd.zadanie.LiczbaPierwsza;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class LiczbaPierwszaParametryzowanyTest {
    @Parameterized.Parameter (value = 0)
    public int liczba;

    @Parameterized.Parameter (value = 1)
    public boolean czyPierwsza;

    @Parameterized.Parameters(name = "{index}: czy liczba {0} powinna byc pierwsza? {1}")
    public static Collection<Object[]> dataProvider() {

        return Arrays.asList(new Object[][]{
                {1, false},
                {2, true},
                {66, false},
                {0, false}});
    }

    @Test
    public void czyLiczbaPierwszaTest(){
        assertEquals(czyPierwsza, LiczbaPierwsza.czyLiczbaPierwsza(liczba));
    }

}