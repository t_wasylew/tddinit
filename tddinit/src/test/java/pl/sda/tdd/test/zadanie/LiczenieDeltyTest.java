package pl.sda.tdd.test.zadanie;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import pl.sda.tdd.zadanie.LiczenieDelty;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class LiczenieDeltyTest {

    @Parameterized.Parameter(value = 0)
    public int a;

    @Parameterized.Parameter(value = 1)
    public int b;

    @Parameterized.Parameter(value = 2)
    public int c;

    @Parameterized.Parameter(value = 3)
    public String expected;

    @Parameterized.Parameters
    public static Collection<Object[]> dataProvider() {

        return Arrays.asList(new Object[][]{
                {10, 1, 1, ""},
                {5, 1, 1, ""},
                {1, 2, 1, "-1"},
                {2, 4, 2, "-1"},
                {-1, 3, 4, "-1,4"}
        });
    }

    @Test
    public void obliczanieDelty() {
        assertEquals(expected, LiczenieDelty.delta(a,b,c));
    }
}
