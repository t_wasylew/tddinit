package pl.sda.tdd.zadanie;

public class LiczenieDelty {
    public static String delta(int a, int b, int c) {
        int delta = (b * b) - 4 * a * c;
        if (delta < 0) {
            return "";
        } else if (delta == 0) {
            return Integer.toString((-b) / (2*a));
        }else {
            return Integer.toString((int)(-b+Math.sqrt(delta))/2*a) + "," + Integer.toString((int) (-b-Math.sqrt(delta))/2*a);
        }
    }
}
