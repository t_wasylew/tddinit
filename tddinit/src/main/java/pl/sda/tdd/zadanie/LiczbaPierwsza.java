package pl.sda.tdd.zadanie;

public class LiczbaPierwsza {
    public static boolean czyLiczbaPierwsza(int i) {
        if (i <= 1) {
            return false;
        }
        for (int j = 2; j <= Math.sqrt(i); j++) {
            if (i % j ==0) {
                return false;
            }
        }
        return true;
    }
}
